#ifndef MAINTHING_H
#define MAINTHING_H

#include <QDialog>

namespace Ui {
class Mainthing;
}

class Mainthing : public QDialog
{
    Q_OBJECT

public:
    explicit Mainthing(QWidget *parent = 0);
    ~Mainthing();

private slots:
    void on_food_clicked();

    void on_thing_clicked();

    void on_season_clicked();

    void on_Profession_clicked();

private:
    Ui::Mainthing *ui;
};

#endif // MAINTHING_H

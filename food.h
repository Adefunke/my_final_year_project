#ifndef FOOD_H
#define FOOD_H

#include <QDialog>

namespace Ui {
class Food;
}

class Food : public QDialog
{
    Q_OBJECT

public:
    explicit Food(QWidget *parent = 0);
    ~Food();

private slots:
    void on_front_clicked();

    void on_back_clicked();

private:
    Ui::Food *ui;
};

#endif // FOOD_H

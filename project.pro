#-------------------------------------------------
#
# Project created by QtCreator 2015-08-24T23:52:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = project
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    food.cpp \
    profession.cpp \
    things.cpp \
    season.cpp \
    foodreal.cpp \
    thindsreal.cpp \
    seasonsreal.cpp \
    professionreal.cpp \
    mainthing.cpp

HEADERS  += mainwindow.h \
    food.h \
    profession.h \
    things.h \
    season.h \
    foodreal.h \
    thindsreal.h \
    seasonsreal.h \
    professionreal.h \
    mainthing.h

FORMS    += mainwindow.ui \
    food.ui \
    profession.ui \
    things.ui \
    season.ui \
    foodreal.ui \
    thindsreal.ui \
    seasonsreal.ui \
    professionreal.ui \
    mainthing.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    project.qrc


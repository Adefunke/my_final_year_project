#include "mainthing.h"
#include "ui_mainthing.h"
#include "food.h"
#include "things.h"
#include "season.h"
#include "profession.h"

Mainthing::Mainthing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Mainthing)
{
    ui->setupUi(this);
}

Mainthing::~Mainthing()
{
    delete ui;
}

void Mainthing::on_food_clicked()
{
    this->hide();
    Food fa;
   fa.setModal(true);
    fa.exec();
}

void Mainthing::on_thing_clicked()
{
    this->hide();
    Things tha;
    tha.setModal(true);
    tha.exec();
}

void Mainthing::on_season_clicked()
{
    this->hide();
    Season sea;
    sea.setModal(true);
    sea.exec();
}

void Mainthing::on_Profession_clicked()
{
    this->hide();
    Profession prof;
    prof.setModal(true);
    prof.exec();
}

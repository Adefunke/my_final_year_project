#ifndef PROFESSIONREAL_H
#define PROFESSIONREAL_H
#include <QTextEdit>
#include <QPlainTextEdit>
#include <QDialog>
using namespace std;

namespace Ui {
class professionreal;
}

class professionreal : public QDialog
{
    Q_OBJECT

public:
    explicit professionreal(QWidget *parent = 0);
    ~professionreal();

private slots:

    void on_a_clicked();

    void on_a_do_clicked();

    void on_a_mi_clicked();

    void on_b_clicked();

    void on_d_clicked();

    void on_e_clicked();

    void on_e_do_clicked();

    void on_e_mi_clicked();

    void on_y_clicked();

    void on_ee_clicked();

    void on_ee_do_clicked();

    void on_ee_mi_clicked();

    void on_f_clicked();

    void on_g_clicked();

    void on_gb_clicked();

    void on_h_clicked();

    void on_i_clicked();

    void on_i_do_clicked();

    void on_i_mi_clicked();

    void on_j_clicked();

    void on_k_clicked();

    void on_l_clicked();

    void on_m_clicked();

    void on_n_clicked();

    void on_o_clicked();

    void on_o_do_clicked();

    void on_o_mi_clicked();

    void on_oo_clicked();

    void on_oo_do_clicked();

    void on_oo_mi_clicked();

    void on_p_clicked();

    void on_r_clicked();

    void on_s_clicked();

    void on_sh_clicked();

    void on_t_clicked();

    void on_u_clicked();

    void on_u_do_clicked();

    void on_u_mi_clicked();

    void on_w_clicked();


    void on_commandLinkButton_clicked();

    void on_pushButton_clicked();

    void on_commandLinkButton_2_clicked();

private:
    Ui::professionreal *ui;
     bool chkBtnOne,chkBtnTwo,chkBtnThree,chkBtnFour;
     bool chkBtnFive,chkBtnSix, chkBtnSeven,chkBtnEight;
     bool chkBtn9,chkBtn10, chkBtn11,chkBtn12;
     bool chkBtn13,chkBtn14, chkBtn15,chkBtn16;
     bool chkBtn20,chkBtn19, chkBtn18,chkBtn17;
     bool chkBtn21,chkBtn22, chkBtn23,chkBtn24;
     bool chkBtn28,chkBtn27, chkBtn26,chkBtn25;
     bool chkBtn29,chkBtn30, chkBtn31,chkBtn32;
     bool chkBtn36,chkBtn35, chkBtn34,chkBtn33;
     bool chkBtn37,chkBtnT38, chkBtn39;
     void randgenerator();
     void data();
     void valuecaptured();
     void damissen();
     void samp();
     void suarez();
     void verifyanswer();
     void verifyspecialanswer();
     void whatif();
};

#endif // PROFESSIONREAL_H

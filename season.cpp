#include "season.h"
#include "ui_season.h"
#include "seasonsreal.h"
#include "mainwindow.h"
#include "mainthing.h"

Season::Season(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Season)
{
    ui->setupUi(this);
}

Season::~Season()
{
    delete ui;
}

void Season::on_front_clicked()
{
    this->hide();
    seasonsreal ses;
   ses.setModal(true);
    ses.exec();
}

void Season::on_back_clicked()
{
    this->hide();
    Mainthing back;
    back.setModal(true);
   back.exec();
}

#ifndef PROFESSION_H
#define PROFESSION_H

#include <QDialog>

namespace Ui {
class Profession;
}

class Profession : public QDialog
{
    Q_OBJECT

public:
    explicit Profession(QWidget *parent = 0);
    ~Profession();

private slots:
    void on_front_clicked();

    void on_back_clicked();

private:
    Ui::Profession *ui;
};

#endif // PROFESSION_H

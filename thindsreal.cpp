#include "thindsreal.h"
#include "ui_thindsreal.h"
#include "things.h"
#include "mainthing.h"
#include "things.h"
#include <QMessageBox>
thindsreal::thindsreal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::thindsreal)
{
    ui->setupUi(this);
    chkBtnOne = chkBtnTwo = false;
    chkBtnThree=chkBtnFour=false;
    chkBtnFive=chkBtnSix=chkBtnSeven=chkBtnEight=false;
    chkBtn9=chkBtn10=chkBtn11=chkBtn12=false;
    chkBtn13=chkBtn14= chkBtn15=chkBtn16=false;
    chkBtn20=chkBtn19=chkBtn18=chkBtn17=false;
    chkBtn21=chkBtn22= chkBtn23=chkBtn24=false;
    chkBtn28=chkBtn27=chkBtn26=chkBtn25=false;
    chkBtn29=chkBtn30=chkBtn31=chkBtn32=false;
    chkBtn36=chkBtn35=chkBtn34=chkBtn33=false;
    chkBtn37=chkBtnT38=chkBtn39=false;
      connect(ui->a,SIGNAL(clicked()),this,SLOT(on_a_clicked()));
      connect(ui->b,SIGNAL(clicked()),this,SLOT(on_b_clicked));
      connect(ui->d,SIGNAL(clicked()),this,SLOT(on_d_clicked()));
      connect(ui->e,SIGNAL(clicked()),this,SLOT(on_e_clicked));
      connect(ui->f,SIGNAL(clicked()),this,SLOT(on_f_clicked()));
      connect(ui->g,SIGNAL(clicked()),this,SLOT(on_g_clicked));
      connect(ui->h,SIGNAL(clicked()),this,SLOT(on_h_clicked()));
      connect(ui->gb,SIGNAL(clicked()),this,SLOT(on_gb_clicked()));
      connect(ui->i,SIGNAL(clicked()),this,SLOT(on_i_clicked));
      connect(ui->j,SIGNAL(clicked()),this,SLOT(on_j_clicked()));
      connect(ui->k,SIGNAL(clicked()),this,SLOT(on_k_clicked));
      connect(ui->l,SIGNAL(clicked()),this,SLOT(on_l_clicked()));
      connect(ui->m,SIGNAL(clicked()),this,SLOT(on_m_clicked));
      connect(ui->n,SIGNAL(clicked()),this,SLOT(on_n_clicked()));
      connect(ui->o,SIGNAL(clicked()),this,SLOT(on_o_clicked));
      connect(ui->p,SIGNAL(clicked()),this,SLOT(on_p_clicked()));
      connect(ui->r,SIGNAL(clicked()),this,SLOT(on_r_clicked));
      connect(ui->s,SIGNAL(clicked()),this,SLOT(on_s_clicked()));
      connect(ui->t,SIGNAL(clicked()),this,SLOT(on_t_clicked));
      connect(ui->u,SIGNAL(clicked()),this,SLOT(on_u_clicked()));
      connect(ui->w,SIGNAL(clicked()),this,SLOT(on_w_clicked));
      connect(ui->y,SIGNAL(clicked()),this,SLOT(on_y_clicked()));
      connect(ui->a_do,SIGNAL(clicked()),this,SLOT(on_a_do_clicked));
      connect(ui->a_mi,SIGNAL(clicked()),this,SLOT(on_a_mi_clicked()));
      connect(ui->e_do,SIGNAL(clicked()),this,SLOT(on_e_do_clicked));
      connect(ui->e_mi,SIGNAL(clicked()),this,SLOT(on_e_mi_clicked()));
      connect(ui->ee,SIGNAL(clicked()),this,SLOT(on_ee_clicked));
      connect(ui->ee_do,SIGNAL(clicked()),this,SLOT(on_ee_do_clicked()));
      connect(ui->ee_mi,SIGNAL(clicked()),this,SLOT(on_ee_mi_clicked));
      connect(ui->o_do,SIGNAL(clicked()),this,SLOT(on_o_do_clicked()));
      connect(ui->o_mi,SIGNAL(clicked()),this,SLOT(on_o_mi_clicked));
      connect(ui->i_do,SIGNAL(clicked()),this,SLOT(on_i_do_clicked()));
      connect(ui->i_mi,SIGNAL(clicked()),this,SLOT(on_i_mi_clicked));
      connect(ui->oo,SIGNAL(clicked()),this,SLOT(on_oo_clicked()));
      connect(ui->oo_do,SIGNAL(clicked()),this,SLOT(on_oo_do_clicked));
      connect(ui->oo_mi,SIGNAL(clicked()),this,SLOT(on_oo_mi_clicked()));
      connect(ui->sh,SIGNAL(clicked()),this,SLOT(on_sh_clicked()));
      connect(ui->u_do,SIGNAL(clicked()),this,SLOT(on_u_do_clicked));
      connect(ui->u_mi,SIGNAL(clicked()),this,SLOT(on_u_mi_clicked()));


    ui->commandLinkButton->setVisible(false);
    ui->widgeta->setVisible(false);
    ui->label_2->setVisible(false);
     ui->label_3->setVisible(false);
      ui->label->setVisible(false);
     ui->emoticon_2->setVisible(false);
    ui->scoringbox->setVisible(false);
    ui->finally->setVisible(false);
    ui->error_display->setVisible(false);
    ui->error_display_2->setVisible(false);
    ui->error_display_3->setVisible(false);
    ui->error_display_4->setVisible(false);
    ui->error_display_5->setVisible(false);
    ui->error_display_6->setVisible(false);
    ui->error_display_7->setVisible(false);
    ui->error_display_8->setVisible(false);
    ui->error_display_9->setVisible(false);
    ui->error_display_10->setVisible(false);
}


thindsreal::~thindsreal()
{
    delete ui;
}
int ac;
int scorec=0;
int totalc=0;
int scorrc=0;
QString valvc=QString::number(scorec);
void thindsreal::on_pushButton_clicked()
{
    ui->pushButton->setVisible(true);
    ui->widgeta->setVisible(true);
    ui->label_2->setVisible(true);
     ui->label_3->setVisible(true);
      ui->label->setVisible(true);
    randgenerator();
    if(totalc==0){
        QMessageBox::information(
            this,
            tr("Instructions"),
            tr("1.This game is designed to spur your interest in Yoruba language.\n "
               "\n2.As u play, if u get to a point where all the words have been guessed"
               " but the game does not show you the next button, just click a button.\n"
               "\n3. Also,know that you have only 10 chances of wrong guesses for a word before the game is over.\n"
               "\n4. Lastly, know that the 'gb' button is not working rather press 'g' and 'b' button separately.\n"
               "\n I wish you all the best while you play!! ") );}
        else{
             }
    ui->pushButton->setVisible(false);
    ui->scorebox->setText(valvc);
    data();
}

void thindsreal::randgenerator()
{
     ac=rand()%32;
}
QString sac;
void thindsreal::data()
{
    QMap<int, QString> Map;
       QMap<int, QString>::iterator it;
       Map[0]= " igi";
       Map[1]= "igb2";
       Map[2]= "asBCbora";
       Map[3]= "7l5kFn";
       Map[4]= "iyFn";
       Map[5]= "E5gi";
       Map[6]= "ad4";
       Map[7]= "BkD ";
       Map[8]= "1d2";
       Map[9]= "7bBn";
       Map[10]= "T1kGt4";
       Map[11]= "7d1";
       Map[12]= "orFn";
       Map[13]= "1p2ta";
       Map[14]= "1bD";
       Map[15]= "1b6 ";
       Map[16]= "1r0";
       Map[17]= "agogo";
       Map[18]= "1pamAwA";
       Map[19]= "gorodAmF";
       Map[20]= "p2t2kA";
       Map[21]= "7l5k5";
       Map[22]= "Cb0";
       Map[23]= "1p9";
       Map[24]= "BlB";
       Map[25]= "9kFn";
       Map[26]= "Bta";
       Map[27]= "Cf1";
       Map[28]= "7w4";
       Map[29]= "1ga";
       Map[30]= "ohun7j1";
       Map[31]= "agolo";
       Map[32]= "ago";

        it = Map.find(ac);
        sac = Map.value(ac);
        valuecaptured();

}
QString countert;
 int  missesa;
QString lettert;
char letnumt;
 void thindsreal::suarez(){
     if(missesa==1){
         ui->error_display->setVisible(true);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesa==2){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(true);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesa==3){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(true);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesa==4){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(true);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesa==5){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(true);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesa==6){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(true);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesa==7){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(true);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesa==8){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(true);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesa==9){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(true);
         ui->error_display_10->setVisible(false);
     }else if(missesa==10){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(true);
     }
 }
 void thindsreal::samp(){
     ui->error_display->setVisible(false);
     ui->error_display_2->setVisible(false);
     ui->error_display_3->setVisible(false);
     ui->error_display_4->setVisible(false);
     ui->error_display_5->setVisible(false);
     ui->error_display_6->setVisible(false);
     ui->error_display_7->setVisible(false);
     ui->error_display_8->setVisible(false);
     ui->error_display_9->setVisible(false);
     ui->error_display_10->setVisible(false);
 }
void thindsreal::valuecaptured()
{
     missesa=0;
    countert="";
    for(int i=0;i<sac.size();i++){
        countert.insert(0,QString("*"));
    }
    ui->general->setText(countert);
}
void thindsreal::verifyanswer(){
    if(sac.contains(lettert)){
        for(int q=0;q<sac.size();q++){
            int h=sac.indexOf(lettert,q);
            countert.replace( h,1,lettert);
            ui->general->setText(countert);
        }
}else{
        missesa++;
        if(missesa>=10){
            damissen();

        }
        else {

            suarez();
        }
    }
}

void thindsreal::verifyspecialanswer(){
    if(sac.contains(letnumt)){
        for(int q=0;q<sac.size();q++){
            int h=sac.indexOf(letnumt,q);
            countert.replace( h,1,lettert);
            ui->general->setText(countert);
        }
}else{
        missesa++;
        if(missesa>=10){
            damissen();
        }
        else {
            suarez();
        }
    }
}
void thindsreal::on_commandLinkButton_clicked()
{
    scorrc=scorrc+scorec;
    totalc=totalc+scorrc;
    ui->scorebox->setVisible(false);
     ui->widgeta->setVisible(true);
    ui->scoringbox->setVisible(true);
    ui->commandLinkButton->setVisible(false);
    QString valu=QString::number(totalc);
    ui->scoringbox->setText(valu);
    countert="";
    on_pushButton_clicked();
}

void thindsreal::damissen()
{
    ui->widgeta->setVisible(false);
    missesa=0;
    ui->finally->setVisible(true);
    scorrc=scorrc+scorec;
    totalc=totalc+scorrc;
    samp();
    ui->commandLinkButton_2->setVisible(true);
    ui->scoringbox->setVisible(true);
    ui->commandLinkButton->setVisible(false);
    QString valu=QString::number(totalc);
    if(totalc>100){
    QMessageBox::information(
        this,
        tr("Level failed"),
        tr("Eeyah!!!, I feel your pain") );}
    else{
        QMessageBox::information(
            this,
            tr("Level failed"),
            tr("Eeyah!!!, I didnt know your Yoruba was this poor") );
    }
    ui->finally->setText("Your totalc scorec is: "+valu);

}
void thindsreal::on_a_clicked()
{
    chkBtnOne=true;
    QString s = QChar(0x0061);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(countert.contains('*')) {
             lettert='a';
             verifyanswer();
         }
     else{
     whatif();
  }
}

void thindsreal::on_b_clicked()
{
    chkBtnTwo=true;
    QString s = QChar(0x0062);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='b';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_d_clicked()
{
    chkBtnThree=true;
    QString s = QChar(0x0064);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='d';
     verifyanswer();}
     else{whatif();
         }
}

void thindsreal::on_e_clicked()
{
    chkBtnFour=true;
    QString s = QChar(0x0065);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='e';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_f_clicked()
{
    chkBtnFive=true;
    QString s = QChar(0x0066);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='f';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_g_clicked()
{
    chkBtnSix=true;
    QString s = QChar(0x0067);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='g';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_gb_clicked()
{
    chkBtnSeven=true;
    QString s = QChar();
    ui->guessbox->setText(s);

}

void thindsreal::on_h_clicked()
{
    chkBtnEight=true;
    QString s = QChar(0x0068);
    ui->guessbox->setText(s);
    if(countert.contains('*')) {
    lettert='h';
    verifyanswer();}
    else{whatif();
        }
}
void thindsreal::on_i_clicked()
{
    chkBtn9=true;
    QString s = QChar(0x0069);
    ui->guessbox->setText(s);
    if(countert.contains('*')) {
    lettert='i';
    verifyanswer();}
    else{whatif();
        }
    ;
}
void thindsreal::on_j_clicked()
{
    chkBtn10=true;
    QString s = QChar(0x006A);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='j';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_k_clicked()
{
    chkBtn11=true;
    QString s = QChar(0x006B);
    ui->guessbox->setText(s);
    if(countert.contains('*')) {
    lettert='k';
    verifyanswer();}
    else{whatif();
        }

}

void thindsreal::on_l_clicked()
{
    chkBtn12=true;
    QString s = QChar(0x006C);
    ui->guessbox->setText(s);
    if(countert.contains('*')) {
    lettert='l';
    verifyanswer();}
    else{whatif();
        }

}

void thindsreal::on_m_clicked()
{
    chkBtn13=true;
    QString s = QChar(0x006D);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='m';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_n_clicked()
{
    chkBtn14=true;
    QString s = QChar(0x006E);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='n';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_o_clicked()
{
    chkBtn15=true;
    QString s = QChar(0x006F);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='o';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_p_clicked()
{
     chkBtn16=true;
    QString s = QChar(0x0070);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='p';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_r_clicked()
{
    chkBtn17=true;
    QString s = QChar(0x0072);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='r';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_s_clicked()
{
    chkBtn18=true;
    QString s = QChar(0x0073);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='s';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_t_clicked()
{
    chkBtn19=true;
    QString s = QChar(0x0074);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='t';
     verifyanswer();}
     else{whatif();
         }
}

void thindsreal::on_u_clicked()
{
    chkBtn20=true;
    QString s = QChar(0x0075);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='u';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_w_clicked()
{
    chkBtn21=true;
    QString s = QChar(0x0077);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='w';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_y_clicked()
{
    chkBtn22=true;
    QString s = QChar(0x0079);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
     lettert='y';
     verifyanswer();}
     else{whatif();
         }

}

void thindsreal::on_a_do_clicked()
{
    chkBtn23=true;
    QString s = QChar(0x00E0);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x00E0);
         letnumt='1';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void thindsreal::on_a_mi_clicked()
{
    chkBtn24=true;
    QString s = QChar(0x00E1);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x00E1);
         letnumt='2';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void thindsreal::on_e_do_clicked()
{
    chkBtn25=true;
    QString s = QChar(0x00E8);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x00E8);
         letnumt='3';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void thindsreal::on_e_mi_clicked()
{
    chkBtn26=true;
    QString s = QChar(0x00E9);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x00E9);
         letnumt='4';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void thindsreal::on_ee_clicked()
{
    chkBtn27=true;
    QString s = QChar(0x1EB9);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x1EB9);
         letnumt='0';
         verifyspecialanswer();}
     else{whatif();
         }

}

void thindsreal::on_ee_do_clicked()
{
    chkBtn28=true;
   // QString s = QChar(0x1EB0);
     ui->guessbox->setText("ẹ̀");
     if(countert.contains('*')) {
         lettert="ẹ̀";
         letnumt='5';
         verifyspecialanswer();;}
     else{whatif();
         }

}

void thindsreal::on_ee_mi_clicked()
{
    chkBtn29=true;
    //QString s = QChar(0x00E0);
     ui->guessbox->setText("ẹ́");
     if(countert.contains('*')) {
         lettert="ẹ́";
         letnumt='6';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void thindsreal::on_i_do_clicked()
{
    chkBtn30=true;
    QString s = QChar(0x00EC);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x00EC);
          letnumt='7';
          verifyspecialanswer();
     }
     else{whatif();
         }

}

void thindsreal::on_i_mi_clicked()
{
    chkBtn31=true;
    QString s = QChar(0x00ED);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x00ED);
         letnumt='8';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void thindsreal::on_o_do_clicked()
{
   chkBtn32=true;
    QString s = QChar(0x00F2);
     ui->guessbox->setText(s);
     // valuecaptured();
     if(countert.contains('*')) {
         lettert=QChar (0x00F2);
         letnumt='9';
         verifyspecialanswer();
     }
     else{
      whatif();
      }
}
void thindsreal::on_o_mi_clicked()
{
    chkBtn33=true;
    QString s = QChar(0x00F3);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x00F3);
         letnumt='A';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void thindsreal::on_oo_clicked()
{
    chkBtn34=true;
    QString s = QChar(0x1ECD);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x1ECD);
         letnumt='B';
         verifyspecialanswer();}
     else{whatif();
         }

}
void thindsreal::on_oo_do_clicked()
{
    chkBtn35=true;
    //QString s = QChar(0x00E0);
     ui->guessbox->setText("ọ̀");
     if(countert.contains('*')) {
         lettert="ọ̀";
         letnumt='C';
         verifyspecialanswer();}
     else{whatif();
         }

}

void thindsreal::on_oo_mi_clicked()
{
    chkBtn36=true;
   // QString s = QChar(0x00F4);
    //QString s =QString(0x1ECD)+QString(0x00B4);
     ui->guessbox->setText("ọ́");
     if(countert.contains('*')) {
         lettert="ọ́";
         letnumt='D';
         verifyspecialanswer();}
     else{whatif();
         }

}
void thindsreal::on_sh_clicked()
{
    chkBtn37=true;
    QString s = QChar(0x1E63);
     ui->guessbox->setText(s);
     if(countert.contains('*')) {
         lettert=QChar (0x1E63);
         letnumt='E';
         verifyspecialanswer();}
     else{whatif(); }
}

void thindsreal::on_u_do_clicked()
{
    chkBtnT38=true;
    QString s = QChar(0x00F9);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(countert.contains('*')) {
         lettert=QChar (0x00F9);
         letnumt='F';
         verifyspecialanswer();
     }
     else{
         whatif();
     }
}

void thindsreal::on_u_mi_clicked()
{
    chkBtn39=true;
    QString s = QChar(0x00FA);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(countert.contains('*')) {
         lettert=QChar (0x00FA);
         letnumt='G';
         verifyspecialanswer();
     }
     else{
         whatif();
     }
}
void thindsreal::whatif(){
    scorec=+(sac.size()*5)- missesa;
    QString valvb=QString::number(scorec);
    ui->scorebox->setText(valvb);
     ui->widgeta->setVisible(false);
    //scorrc=scorec;
    totalc=scorrc;
    if(totalc>=100){
        QMessageBox::information(
            this,
            tr("Word correctly guessed"),
            tr("For getting over 100, you must be a genius!!!") );
    }
    else if(totalc<100){
        QMessageBox::information(
            this,
            tr("Word correctly guessed"),
            tr("That was great!!!") );
    }
   ui->commandLinkButton->setVisible(true);
}

void thindsreal::on_commandLinkButton_2_clicked()
{
    this->hide();
    Things back;
    scorec=0;
    totalc=0;
    scorrc=0;
    back.setModal(true);
    back.exec();
}

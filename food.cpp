#include "food.h"
#include "ui_food.h"
#include "foodreal.h"
#include "mainwindow.h"
#include "mainthing.h"


Food::Food(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Food)
{
    ui->setupUi(this);
}

Food::~Food()
{
    delete ui;
}

void Food::on_front_clicked()
{
    this->hide();
    foodreal front;
   front.setModal(true);
    front.exec();
}

void Food::on_back_clicked()
{
    this->hide();
    Mainthing back;
    back.setModal(true);
   back.exec();
}

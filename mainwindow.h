#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_food_clicked();

    void on_thing_clicked();

    void on_season_clicked();

    void on_Profession_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

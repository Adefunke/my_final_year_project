#include "foodreal.h"
#include "ui_foodreal.h"
#include "mainthing.h"
#include "food.h"
#include <QMessageBox>

foodreal::foodreal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::foodreal)
{
    ui->setupUi(this);
    chkBtnOne = chkBtnTwo = false;
    chkBtnThree=chkBtnFour=false;
    chkBtn=false;
    chkBtnFive=chkBtnSix=chkBtnSeven=chkBtnEight=false;
    chkBtn9=chkBtn10=chkBtn11=chkBtn12=false;
    chkBtn13=chkBtn14= chkBtn15=chkBtn16=false;
    chkBtn20=chkBtn19=chkBtn18=chkBtn17=false;
    chkBtn21=chkBtn22= chkBtn23=chkBtn24=false;
    chkBtn28=chkBtn27=chkBtn26=chkBtn25=false;
    chkBtn29=chkBtn30=chkBtn31=chkBtn32=false;
    chkBtn36=chkBtn35=chkBtn34=chkBtn33=false;
    chkBtn37=chkBtnT38=chkBtn39=false;
      connect(ui->a,SIGNAL(clicked()),this,SLOT(on_a_clicked()));
      connect(ui->b,SIGNAL(clicked()),this,SLOT(on_b_clicked));
      connect(ui->d,SIGNAL(clicked()),this,SLOT(on_d_clicked()));
      connect(ui->e,SIGNAL(clicked()),this,SLOT(on_e_clicked));
      connect(ui->f,SIGNAL(clicked()),this,SLOT(on_f_clicked()));
      connect(ui->g,SIGNAL(clicked()),this,SLOT(on_g_clicked));
      connect(ui->h,SIGNAL(clicked()),this,SLOT(on_h_clicked()));
      connect(ui->gb,SIGNAL(clicked()),this,SLOT(on_gb_clicked()));
      connect(ui->i,SIGNAL(clicked()),this,SLOT(on_i_clicked));
      connect(ui->j,SIGNAL(clicked()),this,SLOT(on_j_clicked()));
      connect(ui->k,SIGNAL(clicked()),this,SLOT(on_k_clicked));
      connect(ui->l,SIGNAL(clicked()),this,SLOT(on_l_clicked()));
      connect(ui->m,SIGNAL(clicked()),this,SLOT(on_m_clicked));
      connect(ui->n,SIGNAL(clicked()),this,SLOT(on_n_clicked()));
      connect(ui->o,SIGNAL(clicked()),this,SLOT(on_o_clicked));
      connect(ui->p,SIGNAL(clicked()),this,SLOT(on_p_clicked()));
      connect(ui->r,SIGNAL(clicked()),this,SLOT(on_r_clicked));
      connect(ui->s,SIGNAL(clicked()),this,SLOT(on_s_clicked()));
      connect(ui->t,SIGNAL(clicked()),this,SLOT(on_t_clicked));
      connect(ui->u,SIGNAL(clicked()),this,SLOT(on_u_clicked()));
      connect(ui->w,SIGNAL(clicked()),this,SLOT(on_w_clicked));
      connect(ui->y,SIGNAL(clicked()),this,SLOT(on_y_clicked()));
      connect(ui->a_do,SIGNAL(clicked()),this,SLOT(on_a_do_clicked));
      connect(ui->a_mi,SIGNAL(clicked()),this,SLOT(on_a_mi_clicked()));
      connect(ui->e_do,SIGNAL(clicked()),this,SLOT(on_e_do_clicked));
      connect(ui->e_mi,SIGNAL(clicked()),this,SLOT(on_e_mi_clicked()));
      connect(ui->ee,SIGNAL(clicked()),this,SLOT(on_ee_clicked));
      connect(ui->ee_do,SIGNAL(clicked()),this,SLOT(on_ee_do_clicked()));
      connect(ui->ee_mi,SIGNAL(clicked()),this,SLOT(on_ee_mi_clicked));
      connect(ui->o_do,SIGNAL(clicked()),this,SLOT(on_o_do_clicked()));
      connect(ui->o_mi,SIGNAL(clicked()),this,SLOT(on_o_mi_clicked));
      connect(ui->i_do,SIGNAL(clicked()),this,SLOT(on_i_do_clicked()));
      connect(ui->i_mi,SIGNAL(clicked()),this,SLOT(on_i_mi_clicked));
      connect(ui->oo,SIGNAL(clicked()),this,SLOT(on_oo_clicked()));
      connect(ui->oo_do,SIGNAL(clicked()),this,SLOT(on_oo_do_clicked));
      connect(ui->oo_mi,SIGNAL(clicked()),this,SLOT(on_oo_mi_clicked()));
      connect(ui->sh,SIGNAL(clicked()),this,SLOT(on_sh_clicked()));
      connect(ui->u_do,SIGNAL(clicked()),this,SLOT(on_u_do_clicked));
      connect(ui->u_mi,SIGNAL(clicked()),this,SLOT(on_u_mi_clicked()));


    ui->commandLinkButton->setVisible(false);
    ui->scoringbox->setVisible(false);
    ui->emoticon_2->setVisible(false);
    ui->widget_2->setVisible(false);
    ui->label_2->setVisible(false);
     ui->label_3->setVisible(false);
      ui->label->setVisible(false);
    ui->error_display->setVisible(false);
    ui->error_display_2->setVisible(false);
    ui->error_display_3->setVisible(false);
    ui->error_display_4->setVisible(false);
    ui->error_display_5->setVisible(false);
    ui->error_display_6->setVisible(false);
    ui->error_display_7->setVisible(false);
    ui->error_display_8->setVisible(false);
    ui->error_display_9->setVisible(false);
    ui->error_display_10->setVisible(false);
}

foodreal::~foodreal()
{
    delete ui;
}
int a;
int scorea=0;
int totala;
int scorra=0;
QString valv=QString::number(scorea);
void foodreal::on_pushButton_clicked()
{
    ui->widget_2->setVisible(true);
    randgenerator();
    if(totala==0){
        QMessageBox::information(
            this,
            tr("Instructions"),
            tr("1.This game is designed to spur your interest in Yoruba language.\n "
               "\n2.As u play, if u get to a point where all the words have been guessed"
               " but the game does not show you the next button, just click a button.\n"
               "\n3. Also,know that you have only 10 chances of wrong guesses for a word before the game is over.\n"
               "\n4. Lastly, know that the 'gb' button is not working rather press 'g' and 'b' button separately.\n"
               "\n I wish you all the best while you play!! ") );}
        else{
             }
    ui->pushButton->setVisible(false);
    ui->label_2->setVisible(true);
     ui->label_3->setVisible(true);
      ui->label->setVisible(true);
    ui->scorebox->setText(valv);
    data();
}
void foodreal::randgenerator()
{
     a=rand()%40;

}
QString sa;
void foodreal::data()
{
    QMap<int, QString> Map;
       QMap<int, QString>::iterator it;
       Map[0]= "3w1";
       Map[1]= "1gbBn";
       Map[2]= "Bs1n";
       Map[3]= "7res7";
       Map[4]= "Cl5";
       Map[5]= "irG";
       Map[6]= "Cg5d5 ";
       Map[7]= "iy2n";
       Map[8]= "iEu";
       Map[9]= "1gb1do";
       Map[10]= "1k1r1";
       Map[11]= "5w1";
       Map[12]= "7b0p0";
       Map[13]= "gb5g7r7";
       Map[14]= "pGpurG";
       Map[15]= "3kuru";
       Map[16]= "ew4dG";
       Map[17]= "1s2rA";
       Map[18]= "ewGro";
       Map[19]= "1m1l1";
       Map[20]= "1gb2lFmD";
       Map[21]= "5b1";
       Map[22]= "kGl8kGl8";
       Map[23]= "d9d9";
       Map[24]= "fFfG";
       Map[25]= "CdFnkGn";
       Map[26]= "dFndG";
       Map[27]= "1s2l1";
       Map[28]= "Bb5";
       Map[29]= "0yin";
       Map[30]= "0y7n";
       Map[31]= "7gb8";
       Map[32]= "Bs1n";
       Map[33]= "il2";
       Map[34]= "il2al2s3p9";
       Map[35]= "0mu";
       Map[36]= "ob70dun";
       Map[37]= "CjCjC";
       Map[38]= "1n1mD";
       Map[39]= "9g7";
       Map[40]= "0ran";

        it = Map.find(a);
        sa = Map.value(a);
        valuecaptured();

}
int misses=0;
void foodreal::suarez(){
    if(misses==1){
        ui->error_display->setVisible(true);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(false);
    }
    else if(misses==2){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(true);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(false);
    }
    else if(misses==3){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(true);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(false);
    }
    else if(misses==4){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(true);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(false);
    }
    else if(misses==5){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(true);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(false);
    }
    else if(misses==6){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(true);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(false);
    }
    else if(misses==7){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(true);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(false);
    }
    else if(misses==8){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(true);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(false);
    }
    else if(misses==9){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(true);
        ui->error_display_10->setVisible(false);
    }else if(misses==10){
        ui->error_display->setVisible(false);
        ui->error_display_2->setVisible(false);
        ui->error_display_3->setVisible(false);
        ui->error_display_4->setVisible(false);
        ui->error_display_5->setVisible(false);
        ui->error_display_6->setVisible(false);
        ui->error_display_7->setVisible(false);
        ui->error_display_8->setVisible(false);
        ui->error_display_9->setVisible(false);
        ui->error_display_10->setVisible(true);
    }
    else{
        ui->widget_2->setVisible(false);
    }
    //else{samp();}
}
void foodreal::samp(){
    ui->error_display->setVisible(false);
    ui->error_display_2->setVisible(false);
    ui->error_display_3->setVisible(false);
    ui->error_display_4->setVisible(false);
    ui->error_display_5->setVisible(false);
    ui->error_display_6->setVisible(false);
    ui->error_display_7->setVisible(false);
    ui->error_display_8->setVisible(false);
    ui->error_display_9->setVisible(false);
    ui->error_display_10->setVisible(false);
}
QString counterr;
QString letter;
QString letnum;
void foodreal::valuecaptured()
{
    samp();
       misses=0;
       counterr="";
    for(int i=0;i<sa.size();i++){
        counterr.insert(0,QString("*"));
    }
    ui->general->setText(counterr);
}

void foodreal::verifyanswer(){
    if(sa.contains(letter)){
        for(int q=0;q<=sa.size();q++){
            int h=sa.indexOf(letter,q);
            counterr.replace( h,1,letter);
            ui->general->setText(counterr);
        }
}else{
        misses++;
        if(misses>10){
            damissen();

        }
        else {

            suarez();
        }
    }
}

void foodreal::verifyspecialanswer(){
    if(sa.contains(letnum)){
        for(int q=0;q<sa.size();q++){
            int h=sa.indexOf(letnum,q);
            counterr.replace(h,1,letter);
            ui->general->setText(counterr);
        }
}else{
        misses++;
        if(misses>10){
            damissen();

        }
        else {

            suarez();
        }
    }
}
void foodreal::on_commandLinkButton_clicked()
{
    //scorra=+scorea;
    totala=totala+scorea;
    misses=0;
    samp();
    ui->scorebox->setVisible(false);
    ui->scoringbox->setVisible(true);
    ui->commandLinkButton->setVisible(false);
    QString valu=QString::number(totala);
    ui->scoringbox->setText(valu);
    on_pushButton_clicked();
}
void foodreal::damissen()
{
    ui->widget_2->setVisible(false);
    misses=0;
    scorra=scorra+scorea;
    totala=totala+scorra;
    ui->scoringbox->setVisible(true);
    ui->commandLinkButton->setVisible(false);
    QString valu=QString::number(totala);
    if(totala>100){
        QMessageBox::information(
            this,
            tr("Level failed"),
            tr("Eeyah!!!, I feel your pain") );}
        else{
            QMessageBox::information(
                this,
                tr("Level failed"),
                tr("Eeyah!!!, I didnt know your Yoruba was this poor") );
        }
    ui->finally->setText("Your total score is: "+valu);
}

void foodreal::on_a_clicked()
{
    chkBtnOne=true;
    QString s = QChar(0x0061);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counterr.contains('*')) {
             letter='a';
             verifyanswer();
         }
     else{
     whatif();
  }
}

void foodreal::on_b_clicked()
{
    chkBtnTwo=true;
    QString s = QChar(0x0062);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='b';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_d_clicked()
{
    chkBtnThree=true;
    QString s = QChar(0x0064);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='d';
     verifyanswer();}
     else{whatif();
         }
}

void foodreal::on_e_clicked()
{
    chkBtnFour=true;
    QString s = QChar(0x0065);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='e';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_f_clicked()
{
    chkBtnFive=true;
    QString s = QChar(0x0066);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='f';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_g_clicked()
{
    chkBtnSix=true;
    QString s = QChar(0x0067);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='g';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_gb_clicked()
{
    chkBtnSeven=true;
    QString s = QChar();
    ui->guessbox->setText(s);

}

void foodreal::on_h_clicked()
{
    chkBtnEight=true;
    QString s = QChar(0x0068);
    ui->guessbox->setText(s);
    if(counterr.contains('*')) {
    letter='h';
    verifyanswer();}
    else{whatif();
        }
}
void foodreal::on_i_clicked()
{
    chkBtn9=true;
    QString s = QChar(0x0069);
    ui->guessbox->setText(s);
    if(counterr.contains('*')) {
    letter='i';
    verifyanswer();}
    else{whatif();
        }
    ;
}
void foodreal::on_j_clicked()
{
    chkBtn10=true;
    QString s = QChar(0x006A);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='j';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_k_clicked()
{
    chkBtn11=true;
    QString s = QChar(0x006B);
    ui->guessbox->setText(s);
    if(counterr.contains('*')) {
    letter='k';
    verifyanswer();}
    else{whatif();
        }

}

void foodreal::on_l_clicked()
{
    chkBtn12=true;
    QString s = QChar(0x006C);
    ui->guessbox->setText(s);
    if(counterr.contains('*')) {
    letter='l';
    verifyanswer();}
    else{whatif();
        }

}

void foodreal::on_m_clicked()
{
    chkBtn13=true;
    QString s = QChar(0x006D);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='m';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_n_clicked()
{
    chkBtn14=true;
    QString s = QChar(0x006E);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='n';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_o_clicked()
{
    chkBtn15=true;
    QString s = QChar(0x006F);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='o';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_p_clicked()
{
     chkBtn16=true;
    QString s = QChar(0x0070);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='p';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_r_clicked()
{
    chkBtn17=true;
    QString s = QChar(0x0072);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='r';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_s_clicked()
{
    chkBtn18=true;
    QString s = QChar(0x0073);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='s';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_t_clicked()
{
    chkBtn19=true;
    QString s = QChar(0x0074);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='t';
     verifyanswer();}
     else{whatif();
         }
}

void foodreal::on_u_clicked()
{
    chkBtn20=true;
    QString s = QChar(0x0075);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='u';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_w_clicked()
{
    chkBtn21=true;
    QString s = QChar(0x0077);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='w';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_y_clicked()
{
    chkBtn22=true;
    QString s = QChar(0x0079);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
     letter='y';
     verifyanswer();}
     else{whatif();
         }

}

void foodreal::on_a_do_clicked()
{
    chkBtn23=true;
    QString s = QChar(0x00E0);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x00E0);
         letnum='1';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void foodreal::on_a_mi_clicked()
{
    chkBtn24=true;
    QString s = QChar(0x00E1);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x00E1);
         letnum='2';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void foodreal::on_e_do_clicked()
{
    chkBtn25=true;
    QString s = QChar(0x00E8);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x00E8);
         letnum='3';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void foodreal::on_e_mi_clicked()
{
    chkBtn26=true;
    QString s = QChar(0x00E9);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x00E9);
         letnum='4';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void foodreal::on_ee_clicked()
{
    chkBtn27=true;
    QString s = QChar(0x1EB9);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x1EB9);
         letnum='0';
         verifyspecialanswer();}
     else{whatif();
         }

}

void foodreal::on_ee_do_clicked()
{
    chkBtn28=true;
   // QString s = QChar(0x1EB0);
     ui->guessbox->setText("ẹ̀");
     if(counterr.contains('*')) {
         letter="ẹ̀";
         letnum='5';
         verifyspecialanswer();;}
     else{whatif();
         }

}

void foodreal::on_ee_mi_clicked()
{
    chkBtn29=true;
    //QString s = QChar(0x00E0);
     ui->guessbox->setText("ẹ́");
     if(counterr.contains('*')) {
         letter="ẹ́";
         letnum='6';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void foodreal::on_i_do_clicked()
{
    chkBtn30=true;
    QString s = QChar(0x00EC);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x00EC);
          letnum='7';
          verifyspecialanswer();
     }
     else{whatif();
         }

}

void foodreal::on_i_mi_clicked()
{
    chkBtn31=true;
    QString s = QChar(0x00ED);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x00ED);
         letnum='8';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void foodreal::on_o_do_clicked()
{
   chkBtn32=true;
    QString s = QChar(0x00F2);
     ui->guessbox->setText(s);
     // valuecaptured();
     if(counterr.contains('*')) {
         letter=QChar (0x00F2);
         letnum='9';
         verifyspecialanswer();
     }
     else{
      whatif();
      }
}
void foodreal::on_o_mi_clicked()
{
    chkBtn33=true;
    QString s = QChar(0x00F3);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x00E0);
         letnum='A';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void foodreal::on_oo_clicked()
{
    chkBtn34=true;
    QString s = QChar(0x1ECD);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x1ECD);
         letnum='B';
         verifyspecialanswer();}
     else{whatif();
         }

}
void foodreal::on_oo_do_clicked()
{
    chkBtn35=true;
    //QString s = QChar(0x00E0);
     ui->guessbox->setText("ọ̀");
     if(counterr.contains('*')) {
         letter="ọ̀";
         letnum='C';
         verifyspecialanswer();}
     else{whatif();
         }

}

void foodreal::on_oo_mi_clicked()
{
    chkBtn36=true;
   // QString s = QChar(0x00F4);
    //QString s =QString(0x1ECD)+QString(0x00B4);
     ui->guessbox->setText("ọ́");
     if(counterr.contains('*')) {
         letter="ọ́";
         letnum='D';
         verifyspecialanswer();}
     else{whatif();
         }

}
void foodreal::on_sh_clicked()
{
    chkBtn37=true;
    QString s = QChar(0x1E63);
     ui->guessbox->setText(s);
     if(counterr.contains('*')) {
         letter=QChar (0x1E63);
         letnum='E';
         verifyspecialanswer();}
     else{whatif(); }
}

void foodreal::on_u_do_clicked()
{
    chkBtnT38=true;
    QString s = QChar(0x00F9);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counterr.contains('*')) {
         letter=QChar (0x00F9);
         letnum='F';
         verifyspecialanswer();
     }
     else{
         whatif();
     }
}

void foodreal::on_u_mi_clicked()
{
    chkBtn39=true;
    QString s = QChar(0x00FA);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counterr.contains('*')) {
         letter=QChar (0x00FA);
         letnum='G';
         verifyspecialanswer();
     }
     else{
         whatif();
     }
}

void foodreal::on_commandLinkButton_2_clicked()
{
    this->hide();
    Food back;
    scorea=0;
    totala=0;
    scorra=0;
    back.setModal(true);
   back.exec();
}
void foodreal::whatif(){
    scorea=scorea+(sa.size()*5)- misses;
    QString valvb=QString::number(scorea);
    ui->scorebox->setText(valvb);
     ui->widget_2->setVisible(false);
    //scorra=scorea;
    totala=scorra;

    if(totala>=100){
        QMessageBox::information(
            this,
            tr("Word correctly guessed"),
            tr("For getting over 100, you must be a genius!!!") );
    }
    else if(totala<100){
        QMessageBox::information(
            this,
            tr("Word correctly guessed"),
            tr("That was great!!!") );
    }

   ui->commandLinkButton->setVisible(true);
}

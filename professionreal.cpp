#include "professionreal.h"
#include "ui_professionreal.h"
#include "mainthing.h"
#include <iostream>
#include <string>
#include <QTextEdit>
#include <QPlainTextEdit>
#include "profession.h"
#include <QMessageBox>
//using namespace std;

professionreal::professionreal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::professionreal)
{
    ui->setupUi(this);
    chkBtnOne = chkBtnTwo = false;
    chkBtnThree=chkBtnFour=false;
    chkBtnFive=chkBtnSix=chkBtnSeven=chkBtnEight=false;
    chkBtn9=chkBtn10=chkBtn11=chkBtn12=false;
    chkBtn13=chkBtn14= chkBtn15=chkBtn16=false;
    chkBtn20=chkBtn19=chkBtn18=chkBtn17=false;
    chkBtn21=chkBtn22= chkBtn23=chkBtn24=false;
    chkBtn28=chkBtn27=chkBtn26=chkBtn25=false;
    chkBtn29=chkBtn30=chkBtn31=chkBtn32=false;
    chkBtn36=chkBtn35=chkBtn34=chkBtn33=false;
    chkBtn37=chkBtnT38=chkBtn39=false;
      connect(ui->a,SIGNAL(clicked()),this,SLOT(on_a_clicked()));
      connect(ui->b,SIGNAL(clicked()),this,SLOT(on_b_clicked));
      connect(ui->d,SIGNAL(clicked()),this,SLOT(on_d_clicked()));
      connect(ui->e,SIGNAL(clicked()),this,SLOT(on_e_clicked));
      connect(ui->f,SIGNAL(clicked()),this,SLOT(on_f_clicked()));
      connect(ui->g,SIGNAL(clicked()),this,SLOT(on_g_clicked));
      connect(ui->h,SIGNAL(clicked()),this,SLOT(on_h_clicked()));
      connect(ui->gb,SIGNAL(clicked()),this,SLOT(on_gb_clicked()));
      connect(ui->i,SIGNAL(clicked()),this,SLOT(on_i_clicked));
      connect(ui->j,SIGNAL(clicked()),this,SLOT(on_j_clicked()));
      connect(ui->k,SIGNAL(clicked()),this,SLOT(on_k_clicked));
      connect(ui->l,SIGNAL(clicked()),this,SLOT(on_l_clicked()));
      connect(ui->m,SIGNAL(clicked()),this,SLOT(on_m_clicked));
      connect(ui->n,SIGNAL(clicked()),this,SLOT(on_n_clicked()));
      connect(ui->o,SIGNAL(clicked()),this,SLOT(on_o_clicked));
      connect(ui->p,SIGNAL(clicked()),this,SLOT(on_p_clicked()));
      connect(ui->r,SIGNAL(clicked()),this,SLOT(on_r_clicked));
      connect(ui->s,SIGNAL(clicked()),this,SLOT(on_s_clicked()));
      connect(ui->t,SIGNAL(clicked()),this,SLOT(on_t_clicked));
      connect(ui->u,SIGNAL(clicked()),this,SLOT(on_u_clicked()));
      connect(ui->w,SIGNAL(clicked()),this,SLOT(on_w_clicked));
      connect(ui->y,SIGNAL(clicked()),this,SLOT(on_y_clicked()));
      connect(ui->a_do,SIGNAL(clicked()),this,SLOT(on_a_do_clicked));
      connect(ui->a_mi,SIGNAL(clicked()),this,SLOT(on_a_mi_clicked()));
      connect(ui->e_do,SIGNAL(clicked()),this,SLOT(on_e_do_clicked));
      connect(ui->e_mi,SIGNAL(clicked()),this,SLOT(on_e_mi_clicked()));
      connect(ui->ee,SIGNAL(clicked()),this,SLOT(on_ee_clicked));
      connect(ui->ee_do,SIGNAL(clicked()),this,SLOT(on_ee_do_clicked()));
      connect(ui->ee_mi,SIGNAL(clicked()),this,SLOT(on_ee_mi_clicked));
      connect(ui->o_do,SIGNAL(clicked()),this,SLOT(on_o_do_clicked()));
      connect(ui->o_mi,SIGNAL(clicked()),this,SLOT(on_o_mi_clicked));
      connect(ui->i_do,SIGNAL(clicked()),this,SLOT(on_i_do_clicked()));
      connect(ui->i_mi,SIGNAL(clicked()),this,SLOT(on_i_mi_clicked));
      connect(ui->oo,SIGNAL(clicked()),this,SLOT(on_oo_clicked()));
      connect(ui->oo_do,SIGNAL(clicked()),this,SLOT(on_oo_do_clicked));
      connect(ui->oo_mi,SIGNAL(clicked()),this,SLOT(on_oo_mi_clicked()));
      connect(ui->sh,SIGNAL(clicked()),this,SLOT(on_sh_clicked()));
      connect(ui->u_do,SIGNAL(clicked()),this,SLOT(on_u_do_clicked));
      connect(ui->u_mi,SIGNAL(clicked()),this,SLOT(on_u_mi_clicked()));

       ui->widget_2->setVisible(false);
       ui->emoticon_2->setVisible(false);
    ui->commandLinkButton->setVisible(false);
    ui->label_2->setVisible(false);
     ui->label_3->setVisible(false);
      ui->label->setVisible(false);
    ui->scoringbox->setVisible(false);
    ui->finally->setVisible(false);
    ui->error_display->setVisible(false);
    ui->error_display_2->setVisible(false);
    ui->error_display_3->setVisible(false);
    ui->error_display_4->setVisible(false);
    ui->error_display_5->setVisible(false);
    ui->error_display_6->setVisible(false);
    ui->error_display_7->setVisible(false);
    ui->error_display_8->setVisible(false);
    ui->error_display_9->setVisible(false);
    ui->error_display_10->setVisible(false);
}

professionreal::~professionreal()
{
    delete ui;
}
int aa;
int score=0;
int total;
int scorr=0;
QString valva=QString::number(score);
void professionreal::on_pushButton_clicked()
{
    ui->widget_2->setVisible(true);
    ui->label_2->setVisible(true);
     ui->label_3->setVisible(true);
      ui->label->setVisible(true);
    randgenerator();
    if(total==0){
        QMessageBox::information(
            this,
            tr("Instructions"),
            tr("1.This game is designed to spur your interest in Yoruba language.\n "
               "\n2.As u play, if u get to a point where all the words have been guessed"
               " but the game does not show you the next button, just click a button.\n"
               "\n3. Also,know that you have only 10 chances of wrong guesses for a word before the game is over.\n"
               "\n4. Lastly, know that the 'gb' button is not working rather press 'g' and 'b' button separately.\n"
               "\n I wish you all the best while you play!! ") );}
        else{
             }
    ui->pushButton->setVisible(false);
    ui->scorebox->setText(valva);
    data();
}

void professionreal::randgenerator()
{
     aa=rand()%15;
}
QString saa;
void professionreal::data()
{
    QMap<int, QString> Map;
       QMap<int, QString>::iterator it;
       Map[0]= "ap0ja";
       Map[1]= "1gb5";
       Map[2]= "BlDd0";
       Map[3]= "olFkB";
       Map[4]= "gb6n1gb6n1";
       Map[5]= "on8d7r8";
       Map[6]= "dAk8t1";
       Map[7]= "olAgFn";
       Map[8]= "1lFf21";
       Map[9]= "BlDp12";      
       Map[10]= "BlDj1";
       Map[11]= "0l6ja";
       Map[12]= "al2t0";
       Map[13]= "fBga";
       Map[14]= "al2ta";
       Map[15]= "Bd0";
       //Map[10]= "gb6n1gb6n1";

        it = Map.find(aa);
        saa = Map.value(aa);
        valuecaptured();

}
 int missesb=0;
 void professionreal::suarez(){
     if(missesb==1){
         ui->error_display->setVisible(true);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesb==2){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(true);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesb==3){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(true);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesb==4){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(true);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesb==5){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(true);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesb==6){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(true);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesb==7){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(true);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesb==8){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(true);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missesb==9){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(true);
         ui->error_display_10->setVisible(false);
     }else if(missesb==10){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(true);
     }
     else{
         samp();
     }
 }
 QString letterp;
 QString counter;
 char letnump;
 void professionreal::samp(){
     ui->error_display->setVisible(false);
     ui->error_display_2->setVisible(false);
     ui->error_display_3->setVisible(false);
     ui->error_display_4->setVisible(false);
     ui->error_display_5->setVisible(false);
     ui->error_display_6->setVisible(false);
     ui->error_display_7->setVisible(false);
     ui->error_display_8->setVisible(false);
     ui->error_display_9->setVisible(false);
     ui->error_display_10->setVisible(false);
 }
void professionreal::valuecaptured()
{
    counter="";
      missesb=0;
    for(int i=0;i<saa.size();i++){
        counter.insert(0,QString("*"));
    }
    ui->general->setText(counter);
}

void professionreal::on_commandLinkButton_clicked()
{   //score=scorr;
    missesb=0;
    scorr=+score;
    total+=scorr;
    score=0;
    samp();
    ui->scorebox->setVisible(false);
    ui->scoringbox->setVisible(true);
     ui->widget_2->setVisible(true);
    ui->commandLinkButton->setVisible(false);
    QString valu=QString::number(total);
    ui->scoringbox->setText(valu);
    on_pushButton_clicked();
}
void professionreal::damissen()
{  
    ui->widget_2->setVisible(false);
    missesb=0;
    ui->finally->setVisible(true);
    scorr=+score;
    total+=scorr;
    ui->commandLinkButton_2->setVisible(true);
    ui->scoringbox->setVisible(true);
    ui->commandLinkButton->setVisible(false);
    QString valu=QString::number(total);
    if(total>100){
    QMessageBox::information(
        this,
        tr("Level failed"),
        tr("Eeyah!!!, I feel your pain") );}
    else{
        QMessageBox::information(
            this,
            tr("Level failed"),
            tr("Eeyah!!!, I didnt know your Yoruba was this poor") );
    }
    ui->finally->setText("Your total score is: "+valu);
}
void professionreal::verifyanswer(){
    if(saa.contains(letterp)){
        for(int q=0;q<saa.size();q++){
            int h=saa.indexOf(letterp,q);
            counter.replace( h,1,letterp);
            ui->general->setText(counter);
        }
}else{
         missesb++;
        if( missesb>10){
            damissen();

        }else {
             suarez();
        }
    }
}
void professionreal::verifyspecialanswer(){
    if(saa.contains(letnump)){
        for(int q=0;q<saa.size();q++){
            int h=saa.indexOf(letnump,q);
            counter.replace( h,1,letterp);
            ui->general->setText(counter);
        }
}else{
         missesb++;
        if( missesb>10){
            damissen();

        }else {
             suarez();
        }
    }
}

void professionreal::on_a_clicked()
{
    chkBtnOne=true;
    QString s = QChar(0x0061);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counter.contains('*')) {
             letterp='a';
             verifyanswer();
         }
     else{
     whatif();
  }
}

void professionreal::on_b_clicked()
{
    chkBtnTwo=true;
    QString s = QChar(0x0062);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='b';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_d_clicked()
{
    chkBtnThree=true;
    QString s = QChar(0x0064);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='d';
     verifyanswer();}
     else{whatif();
         }
}

void professionreal::on_e_clicked()
{
    chkBtnFour=true;
    QString s = QChar(0x0065);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='e';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_f_clicked()
{
    chkBtnFive=true;
    QString s = QChar(0x0066);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='f';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_g_clicked()
{
    chkBtnSix=true;
    QString s = QChar(0x0067);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='g';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_gb_clicked()
{
    chkBtnSeven=true;
    QString s = QChar();
    ui->guessbox->setText(s);

}

void professionreal::on_h_clicked()
{
    chkBtnEight=true;
    QString s = QChar(0x0068);
    ui->guessbox->setText(s);
    if(counter.contains('*')) {
    letterp='h';
    verifyanswer();}
    else{whatif();
        }
}
void professionreal::on_i_clicked()
{
    chkBtn9=true;
    QString s = QChar(0x0069);
    ui->guessbox->setText(s);
    if(counter.contains('*')) {
    letterp='i';
    verifyanswer();}
    else{whatif();
        }
    ;
}
void professionreal::on_j_clicked()
{
    chkBtn10=true;
    QString s = QChar(0x006A);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='j';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_k_clicked()
{
    chkBtn11=true;
    QString s = QChar(0x006B);
    ui->guessbox->setText(s);
    if(counter.contains('*')) {
    letterp='k';
    verifyanswer();}
    else{whatif();
        }

}

void professionreal::on_l_clicked()
{
    chkBtn12=true;
    QString s = QChar(0x006C);
    ui->guessbox->setText(s);
    if(counter.contains('*')) {
    letterp='l';
    verifyanswer();}
    else{whatif();
        }

}

void professionreal::on_m_clicked()
{
    chkBtn13=true;
    QString s = QChar(0x006D);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='m';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_n_clicked()
{
    chkBtn14=true;
    QString s = QChar(0x006E);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='n';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_o_clicked()
{
    chkBtn15=true;
    QString s = QChar(0x006F);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='o';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_p_clicked()
{
     chkBtn16=true;
    QString s = QChar(0x0070);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='p';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_r_clicked()
{
    chkBtn17=true;
    QString s = QChar(0x0072);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='r';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_s_clicked()
{
    chkBtn18=true;
    QString s = QChar(0x0073);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='s';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_t_clicked()
{
    chkBtn19=true;
    QString s = QChar(0x0074);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='t';
     verifyanswer();}
     else{whatif();
         }
}

void professionreal::on_u_clicked()
{
    chkBtn20=true;
    QString s = QChar(0x0075);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='u';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_w_clicked()
{
    chkBtn21=true;
    QString s = QChar(0x0077);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='w';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_y_clicked()
{
    chkBtn22=true;
    QString s = QChar(0x0079);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
     letterp='y';
     verifyanswer();}
     else{whatif();
         }

}

void professionreal::on_a_do_clicked()
{
    chkBtn23=true;
    QString s = QChar(0x00E0);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x00E0);
         letnump='1';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void professionreal::on_a_mi_clicked()
{
    chkBtn24=true;
    QString s = QChar(0x00E1);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x00E1);
         letnump='2';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void professionreal::on_e_do_clicked()
{
    chkBtn25=true;
    QString s = QChar(0x00E8);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x00E8);
         letnump='3';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void professionreal::on_e_mi_clicked()
{
    chkBtn26=true;
    QString s = QChar(0x00E9);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x00E9);
         letnump='4';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void professionreal::on_ee_clicked()
{
    chkBtn27=true;
    QString s = QChar(0x1EB9);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x1EB9);
         letnump='0';
         verifyspecialanswer();}
     else{whatif();
         }

}

void professionreal::on_ee_do_clicked()
{
    chkBtn28=true;
   // QString s = QChar(0x1EB0);
     ui->guessbox->setText("ẹ̀");
     if(counter.contains('*')) {
         letterp="ẹ̀";
         letnump='5';
         verifyspecialanswer();;}
     else{whatif();
         }

}

void professionreal::on_ee_mi_clicked()
{
    chkBtn29=true;
    //QString s = QChar(0x00E0);
     ui->guessbox->setText("ẹ́");
     if(counter.contains('*')) {
         letterp="ẹ́";
         letnump='6';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void professionreal::on_i_do_clicked()
{
    chkBtn30=true;
    QString s = QChar(0x00EC);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x00EC);
          letnump='7';
          verifyspecialanswer();
     }
     else{whatif();
         }

}

void professionreal::on_i_mi_clicked()
{
    chkBtn31=true;
    QString s = QChar(0x00ED);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x00ED);
         letnump='8';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void professionreal::on_o_do_clicked()
{
   chkBtn32=true;
    QString s = QChar(0x00F2);
     ui->guessbox->setText(s);
     // valuecaptured();
     if(counter.contains('*')) {
         letterp=QChar (0x00F2);
         letnump='9';
         verifyspecialanswer();
     }
     else{
      whatif();
      }
}
void professionreal::on_o_mi_clicked()
{
    chkBtn33=true;
    QString s = QChar(0x00F3);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x00E0);
         letnump='A';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void professionreal::on_oo_clicked()
{
    chkBtn34=true;
    QString s = QChar(0x1ECD);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x1ECD);
         letnump='B';
         verifyspecialanswer();}
     else{whatif();
         }

}
void professionreal::on_oo_do_clicked()
{
    chkBtn35=true;
    //QString s = QChar(0x00E0);
     ui->guessbox->setText("ọ̀");
     if(counter.contains('*')) {
         letterp='ọ̀';
         letnump='C';
         verifyspecialanswer();}
     else{whatif();
         }

}

void professionreal::on_oo_mi_clicked()
{
    chkBtn36=true;
   // QString s = QChar(0x00F4);
    //QString s =QString(0x1ECD)+QString(0x00B4);
     ui->guessbox->setText("ọ́");
     if(counter.contains('*')) {
         letterp='ọ́';
         letnump='D';
         verifyspecialanswer();}
     else{whatif();
         }

}
void professionreal::on_sh_clicked()
{
    chkBtn37=true;
    QString s = QChar(0x1E63);
     ui->guessbox->setText(s);
     if(counter.contains('*')) {
         letterp=QChar (0x1E63);
         letnump='E';
         verifyspecialanswer();}
     else{whatif(); }
}

void professionreal::on_u_do_clicked()
{
    chkBtnT38=true;
    QString s = QChar(0x00F9);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counter.contains('*')) {
         letterp=QChar (0x00F9);
         letnump='F';
         verifyspecialanswer();
     }
     else{
         whatif();
     }
}

void professionreal::on_u_mi_clicked()
{
    chkBtn39=true;
    QString s = QChar(0x00FA);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counter.contains('*')) {
         letterp=QChar (0x00FA);
         letnump='G';
         verifyspecialanswer();
     }
     else{
         whatif();
     }
}
void professionreal::whatif(){
    score=+(saa.size()*5)- missesb;
    QString valvb=QString::number(score);
    ui->scorebox->setText(valvb);
     ui->widget_2->setVisible(false);
    //scorr=score;
    total=scorr;

    if(total>=100){
        QMessageBox::information(
            this,
            tr("Word correctly guessed"),
            tr("For getting over 100, you must be a genius!!!") );
    }
    else if(total<100){
        QMessageBox::information(
            this,
            tr("Word correctly guessed"),
            tr("That was great!!!") );
    }

   ui->commandLinkButton->setVisible(true);
}

void professionreal::on_commandLinkButton_2_clicked()
{
    this->hide();
    Profession back;  
    score=0;
    total=0;
    scorr=0;
    back.setModal(true);
   back.exec();
}

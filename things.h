#ifndef THINGS_H
#define THINGS_H

#include <QDialog>

namespace Ui {
class Things;
}

class Things : public QDialog
{
    Q_OBJECT

public:
    explicit Things(QWidget *parent = 0);
    ~Things();

private slots:
    void on_front_clicked();

    void on_back_clicked();

private:
    Ui::Things *ui;
};

#endif // THINGS_H

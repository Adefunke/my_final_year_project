#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "food.h"
#include "things.h"
#include "season.h"
#include "profession.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_food_clicked()
{
    this->hide();
    Food fa;
   fa.setModal(true);
    fa.exec();
}

void MainWindow::on_thing_clicked()
{
    this->hide();
    Things tha;
    tha.setModal(true);
    tha.exec();
}

void MainWindow::on_season_clicked()
{
    this->hide();
    Season sea;
    sea.setModal(true);
    sea.exec();
}

void MainWindow::on_Profession_clicked()
{
    this->hide();
    Profession prof;
    prof.setModal(true);
    prof.exec();
}

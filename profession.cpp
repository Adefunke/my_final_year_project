#include "profession.h"
#include "ui_profession.h"
#include "professionreal.h"
#include "mainwindow.h"
#include "mainthing.h"

Profession::Profession(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Profession)
{
    ui->setupUi(this);
}

Profession::~Profession()
{
    delete ui;
}
void Profession::on_front_clicked()
{
    this->hide();
    professionreal front;
   front.setModal(true);
    front.exec();
}

void Profession::on_back_clicked()
{
   this->hide();
   Mainthing back;
   back.setModal(true);
  back.exec();
}

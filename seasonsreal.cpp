#include "seasonsreal.h"
#include "ui_seasonsreal.h"
#include "mainthing.h"
#include "season.h"
#include <QMessageBox>
seasonsreal::seasonsreal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::seasonsreal)
{
    ui->setupUi(this);
    chkBtnOne = chkBtnTwo = false;
    chkBtnThree=chkBtnFour=false;
    chkBtnFive=chkBtnSix=chkBtnSeven=chkBtnEight=false;
    chkBtn9=chkBtn10=chkBtn11=chkBtn12=false;
    chkBtn13=chkBtn14= chkBtn15=chkBtn16=false;
    chkBtn20=chkBtn19=chkBtn18=chkBtn17=false;
    chkBtn21=chkBtn22= chkBtn23=chkBtn24=false;
    chkBtn28=chkBtn27=chkBtn26=chkBtn25=false;
    chkBtn29=chkBtn30=chkBtn31=chkBtn32=false;
    chkBtn36=chkBtn35=chkBtn34=chkBtn33=false;
    chkBtn37=chkBtnT38=chkBtn39=false;
      connect(ui->a,SIGNAL(clicked()),this,SLOT(on_a_clicked()));
      connect(ui->b,SIGNAL(clicked()),this,SLOT(on_b_clicked));
      connect(ui->d,SIGNAL(clicked()),this,SLOT(on_d_clicked()));
      connect(ui->e,SIGNAL(clicked()),this,SLOT(on_e_clicked));
      connect(ui->f,SIGNAL(clicked()),this,SLOT(on_f_clicked()));
      connect(ui->g,SIGNAL(clicked()),this,SLOT(on_g_clicked));
      connect(ui->h,SIGNAL(clicked()),this,SLOT(on_h_clicked()));
      connect(ui->gb,SIGNAL(clicked()),this,SLOT(on_gb_clicked()));
      connect(ui->i,SIGNAL(clicked()),this,SLOT(on_i_clicked));
      connect(ui->j,SIGNAL(clicked()),this,SLOT(on_j_clicked()));
      connect(ui->k,SIGNAL(clicked()),this,SLOT(on_k_clicked));
      connect(ui->l,SIGNAL(clicked()),this,SLOT(on_l_clicked()));
      connect(ui->m,SIGNAL(clicked()),this,SLOT(on_m_clicked));
      connect(ui->n,SIGNAL(clicked()),this,SLOT(on_n_clicked()));
      connect(ui->o,SIGNAL(clicked()),this,SLOT(on_o_clicked));
      connect(ui->p,SIGNAL(clicked()),this,SLOT(on_p_clicked()));
      connect(ui->r,SIGNAL(clicked()),this,SLOT(on_r_clicked));
      connect(ui->s,SIGNAL(clicked()),this,SLOT(on_s_clicked()));
      connect(ui->t,SIGNAL(clicked()),this,SLOT(on_t_clicked));
      connect(ui->u,SIGNAL(clicked()),this,SLOT(on_u_clicked()));
      connect(ui->w,SIGNAL(clicked()),this,SLOT(on_w_clicked));
      connect(ui->y,SIGNAL(clicked()),this,SLOT(on_y_clicked()));
      connect(ui->a_do,SIGNAL(clicked()),this,SLOT(on_a_do_clicked));
      connect(ui->a_mi,SIGNAL(clicked()),this,SLOT(on_a_mi_clicked()));
      connect(ui->e_do,SIGNAL(clicked()),this,SLOT(on_e_do_clicked));
      connect(ui->e_mi,SIGNAL(clicked()),this,SLOT(on_e_mi_clicked()));
      connect(ui->ee,SIGNAL(clicked()),this,SLOT(on_ee_clicked));
      connect(ui->ee_do,SIGNAL(clicked()),this,SLOT(on_ee_do_clicked()));
      connect(ui->ee_mi,SIGNAL(clicked()),this,SLOT(on_ee_mi_clicked));
      connect(ui->o_do,SIGNAL(clicked()),this,SLOT(on_o_do_clicked()));
      connect(ui->o_mi,SIGNAL(clicked()),this,SLOT(on_o_mi_clicked));
      connect(ui->i_do,SIGNAL(clicked()),this,SLOT(on_i_do_clicked()));
      connect(ui->i_mi,SIGNAL(clicked()),this,SLOT(on_i_mi_clicked));
      connect(ui->oo,SIGNAL(clicked()),this,SLOT(on_oo_clicked()));
      connect(ui->oo_do,SIGNAL(clicked()),this,SLOT(on_oo_do_clicked));
      connect(ui->oo_mi,SIGNAL(clicked()),this,SLOT(on_oo_mi_clicked()));
      connect(ui->sh,SIGNAL(clicked()),this,SLOT(on_sh_clicked()));
      connect(ui->u_do,SIGNAL(clicked()),this,SLOT(on_u_do_clicked));
      connect(ui->u_mi,SIGNAL(clicked()),this,SLOT(on_u_mi_clicked()));


    ui->commandLinkButton->setVisible(false);
    ui->widget_2->setVisible(false);
     ui->emoticon_2->setVisible(false);
     ui->label_2->setVisible(false);
      ui->label_3->setVisible(false);
       ui->label->setVisible(false);
    ui->scoringbox->setVisible(false);
    ui->finally->setVisible(false);
    ui->error_display->setVisible(false);
    ui->error_display_2->setVisible(false);
    ui->error_display_3->setVisible(false);
    ui->error_display_4->setVisible(false);
    ui->error_display_5->setVisible(false);
    ui->error_display_6->setVisible(false);
    ui->error_display_7->setVisible(false);
    ui->error_display_8->setVisible(false);
    ui->error_display_9->setVisible(false);
    ui->error_display_10->setVisible(false);
}

seasonsreal::~seasonsreal()
{
    delete ui;
}
int ab;
int scoreb=0;
int totalb;
int scorrb=0;
QString valvb=QString::number(scoreb);
void seasonsreal::on_pushButton_clicked()
{
    ui->label_2->setVisible(true);
     ui->label_3->setVisible(true);
      ui->label->setVisible(true);
      ui->widget_2->setVisible(true);
    randgenerator();
    if(totalb==0){
        QMessageBox::information(
            this,
            tr("Instructions"),
            tr("1.This game is designed to spur your interest in Yoruba language.\n "
               "\n2.As u play, if u get to a point where all the words have been guessed"
               " but the game does not show you the next button, just click a button.\n"
               "\n3. Also,know that you have only 10 chances of wrong guesses for a word before the game is over.\n"
               "\n4. Lastly, know that the 'gb' button is not working rather press 'g' and 'b' button separately.\n"
               "\n I wish you all the best while you play!! ") );}
        else{
             }
    ui->pushButton->setVisible(false);
    ui->scorebox->setText(valvb);
    data();
}
void seasonsreal::randgenerator()
{
     ab=rand()%13;
}
QString sab;
void seasonsreal::data()
{
    QMap<int, QString> Map;
       QMap<int, QString>::iterator it;
       Map[0]= "9j9";
       Map[1]= "9tGtF";
       Map[2]= "0y4";
       Map[3]= "9g7n7nt7";
       Map[4]= "7gb1";
       Map[5]= "9Eogbo";
       Map[6]= "9yA";
       Map[7]= "9Eun";
       Map[8]= "7f5 ";
       Map[9]= "3k7t7";
       Map[10]= "7b1d1n";
       Map[11]= "11fin";
       Map[12]= "Bj1";
       Map[13]= "5wCn";

        it = Map.find(ab);
        sab = Map.value(ab);
        valuecaptured();

}
 int  missesc=0;
 void seasonsreal::suarez(){
     int missing=missesc;
     if(missing==1){
         ui->error_display->setVisible(true);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missing==2){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(true);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missing==3){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(true);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missing==4){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(true);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missing==5){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(true);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missing==6){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(true);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missing==7){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(true);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missing==8){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(true);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(false);
     }
     else if(missing==9){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(true);
         ui->error_display_10->setVisible(false);
     }else if(missing==10){
         ui->error_display->setVisible(false);
         ui->error_display_2->setVisible(false);
         ui->error_display_3->setVisible(false);
         ui->error_display_4->setVisible(false);
         ui->error_display_5->setVisible(false);
         ui->error_display_6->setVisible(false);
         ui->error_display_7->setVisible(false);
         ui->error_display_8->setVisible(false);
         ui->error_display_9->setVisible(false);
         ui->error_display_10->setVisible(true);
     }
     else{samp();}
 }
 void seasonsreal::samp(){
     ui->error_display->setVisible(false);
     ui->error_display_2->setVisible(false);
     ui->error_display_3->setVisible(false);
     ui->error_display_4->setVisible(false);
     ui->error_display_5->setVisible(false);
     ui->error_display_6->setVisible(false);
     ui->error_display_7->setVisible(false);
     ui->error_display_8->setVisible(false);
     ui->error_display_9->setVisible(false);
     ui->error_display_10->setVisible(false);
 }
 QString counters;
 QString letters;
 char letnums;
void seasonsreal::valuecaptured()
{
       missesc=0;
       counters="";
    for(int i=0;i<sab.size();i++){
        counters.insert(0,QString("*"));
    }
    ui->general->setText(counters);
}
void seasonsreal::verifyanswer(){
    if(sab.contains(letters)){
        for(int q=0;q<sab.size();q++){
            int h=sab.indexOf(letters,q);
            counters.replace( h,1,letters);
            ui->general->setText(counters);
        }
}else{
         missesc++;
        if( missesc>10){
             damissen();

        }else {
            suarez();
        }
    }
}
void seasonsreal::verifyspecialanswer(){
    if(sab.contains(letnums)){
        for(int q=0;q<sab.size();q++){
            int h=sab.indexOf(letnums,q);
            counters.replace( h,1,letters);
            ui->general->setText(counters);
        }
}else{
         missesc++;
        if( missesc>10){
             damissen();

        }else {
            suarez();
        }
    }
}

void seasonsreal::damissen()
{
    ui->widget_2->setVisible(false);
    missesc=0;
    ui->finally->setVisible(true);
    scorrb=scorrb+scoreb;
    totalb=totalb+scorrb;
    ui->commandLinkButton_2->setVisible(true);
    ui->scoringbox->setVisible(true);
    ui->commandLinkButton->setVisible(false);
    QString valu=QString::number(totalb);
    if(totalb>100){
    QMessageBox::information(
        this,
        tr("Level failed"),
        tr("Eeyah!!!, I feel your pain") );}
    else{
        QMessageBox::information(
            this,
            tr("Level failed"),
            tr("Eeyah!!!, I didnt know your Yoruba was this poor") );
    }
    ui->finally->setText("Your total score is: "+valu);

}

void seasonsreal::on_commandLinkButton_clicked()
{
    scorrb=scorrb+scoreb;
    totalb+=scorrb;
    samp();
    ui->scorebox->setVisible(false);
    ui->scoringbox->setVisible(true);
     ui->widget_2->setVisible(false);
    ui->commandLinkButton->setVisible(false);
    QString valu=QString::number(totalb);
    ui->scoringbox->setText(valu);
    on_pushButton_clicked();
}

void seasonsreal::on_a_clicked()
{
    chkBtnOne=true;
    QString s = QChar(0x0061);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counters.contains('*')) {
             letters='a';
             verifyanswer();
         }
     else{
     whatif();
  }
}

void seasonsreal::on_b_clicked()
{
    chkBtnTwo=true;
    QString s = QChar(0x0062);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='b';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_d_clicked()
{
    chkBtnThree=true;
    QString s = QChar(0x0064);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='d';
     verifyanswer();}
     else{whatif();
         }
}

void seasonsreal::on_e_clicked()
{
    chkBtnFour=true;
    QString s = QChar(0x0065);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='e';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_f_clicked()
{
    chkBtnFive=true;
    QString s = QChar(0x0066);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='f';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_g_clicked()
{
    chkBtnSix=true;
    QString s = QChar(0x0067);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='g';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_gb_clicked()
{
    chkBtnSeven=true;
    QString s = QChar();
    ui->guessbox->setText(s);

}

void seasonsreal::on_h_clicked()
{
    chkBtnEight=true;
    QString s = QChar(0x0068);
    ui->guessbox->setText(s);
    if(counters.contains('*')) {
    letters='h';
    verifyanswer();}
    else{whatif();
        }
}
void seasonsreal::on_i_clicked()
{
    chkBtn9=true;
    QString s = QChar(0x0069);
    ui->guessbox->setText(s);
    if(counters.contains('*')) {
    letters='i';
    verifyanswer();}
    else{whatif();
        }
    ;
}
void seasonsreal::on_j_clicked()
{
    chkBtn10=true;
    QString s = QChar(0x006A);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='j';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_k_clicked()
{
    chkBtn11=true;
    QString s = QChar(0x006B);
    ui->guessbox->setText(s);
    if(counters.contains('*')) {
    letters='k';
    verifyanswer();}
    else{whatif();
        }

}

void seasonsreal::on_l_clicked()
{
    chkBtn12=true;
    QString s = QChar(0x006C);
    ui->guessbox->setText(s);
    if(counters.contains('*')) {
    letters='l';
    verifyanswer();}
    else{whatif();
        }

}

void seasonsreal::on_m_clicked()
{
    chkBtn13=true;
    QString s = QChar(0x006D);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='m';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_n_clicked()
{
    chkBtn14=true;
    QString s = QChar(0x006E);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='n';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_o_clicked()
{
    chkBtn15=true;
    QString s = QChar(0x006F);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='o';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_p_clicked()
{
     chkBtn16=true;
    QString s = QChar(0x0070);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='p';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_r_clicked()
{
    chkBtn17=true;
    QString s = QChar(0x0072);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='r';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_s_clicked()
{
    chkBtn18=true;
    QString s = QChar(0x0073);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='s';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_t_clicked()
{
    chkBtn19=true;
    QString s = QChar(0x0074);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='t';
     verifyanswer();}
     else{whatif();
         }
}

void seasonsreal::on_u_clicked()
{
    chkBtn20=true;
    QString s = QChar(0x0075);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='u';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_w_clicked()
{
    chkBtn21=true;
    QString s = QChar(0x0077);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='w';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_y_clicked()
{
    chkBtn22=true;
    QString s = QChar(0x0079);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
     letters='y';
     verifyanswer();}
     else{whatif();
         }

}

void seasonsreal::on_a_do_clicked()
{
    chkBtn23=true;
    QString s = QChar(0x00E0);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x00E0);
         letnums='1';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void seasonsreal::on_a_mi_clicked()
{
    chkBtn24=true;
    QString s = QChar(0x00E1);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x00E1);
         letnums='2';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void seasonsreal::on_e_do_clicked()
{
    chkBtn25=true;
    QString s = QChar(0x00E8);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x00E8);
         letnums='3';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void seasonsreal::on_e_mi_clicked()
{
    chkBtn26=true;
    QString s = QChar(0x00E9);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x00E9);
         letnums='4';
         verifyspecialanswer();
     }
     else{whatif();
         }

}

void seasonsreal::on_ee_clicked()
{
    chkBtn27=true;
    QString s = QChar(0x1EB9);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x1EB9);
         letnums='0';
         verifyspecialanswer();}
     else{whatif();
         }

}

void seasonsreal::on_ee_do_clicked()
{
    chkBtn28=true;
   // QString s = QChar(0x1EB0);
     ui->guessbox->setText("ẹ̀");
     if(counters.contains('*')) {
         letters="ẹ̀";
         letnums='5';
         verifyspecialanswer();;}
     else{whatif();
         }

}

void seasonsreal::on_ee_mi_clicked()
{
    chkBtn29=true;
    //QString s = QChar(0x00E0);
     ui->guessbox->setText("ẹ́");
     if(counters.contains('*')) {
         letters="ẹ́";
         letnums='6';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void seasonsreal::on_i_do_clicked()
{
    chkBtn30=true;
    QString s = QChar(0x00EC);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x00EC);
          letnums='7';
          verifyspecialanswer();
     }
     else{whatif();
         }

}

void seasonsreal::on_i_mi_clicked()
{
    chkBtn31=true;
    QString s = QChar(0x00ED);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x00ED);
         letnums='8';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void seasonsreal::on_o_do_clicked()
{
   chkBtn32=true;
    QString s = QChar(0x00F2);
     ui->guessbox->setText(s);
     // valuecaptured();
     if(counters.contains('*')) {
         letters=QChar (0x00F2);
         letnums='9';
         verifyspecialanswer();
     }
     else{
      whatif();
      }
}
void seasonsreal::on_o_mi_clicked()
{
    chkBtn33=true;
    QString s = QChar(0x00F3);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x00E0);
         letnums='A';
         verifyspecialanswer();
     }
     else{whatif();
         }

}
void seasonsreal::on_oo_clicked()
{
    chkBtn34=true;
    QString s = QChar(0x1ECD);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x1ECD);
         letnums='B';
         verifyspecialanswer();}
     else{whatif();
         }

}
void seasonsreal::on_oo_do_clicked()
{
    chkBtn35=true;
    //QString s = QChar(0x00E0);
     ui->guessbox->setText("ọ̀");
     if(counters.contains('*')) {
         letters="ọ̀";
         letnums='C';
         verifyspecialanswer();}
     else{whatif();
         }

}

void seasonsreal::on_oo_mi_clicked()
{
    chkBtn36=true;
   // QString s = QChar(0x00F4);
    //QString s =QString(0x1ECD)+QString(0x00B4);
     ui->guessbox->setText("ọ́");
     if(counters.contains('*')) {
         letters='ọ́';
         letnums='D';
         verifyspecialanswer();}
     else{whatif();
         }

}
void seasonsreal::on_sh_clicked()
{
    chkBtn37=true;
    QString s = QChar(0x1E63);
     ui->guessbox->setText(s);
     if(counters.contains('*')) {
         letters=QChar (0x1E63);
         letnums='E';
         verifyspecialanswer();}
     else{whatif();
         }
}

void seasonsreal::on_u_do_clicked()
{
    chkBtnT38=true;
    QString s = QChar(0x00F9);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counters.contains('*')) {
         letters=QChar (0x00F9);
         letnums='F';
         verifyspecialanswer();
     }
     else{
     whatif();
     }
}

void seasonsreal::on_u_mi_clicked()
{
    chkBtn39=true;
    QString s = QChar(0x00FA);
     ui->guessbox->setText(s);
     //valuecaptured();
     if(counters.contains('*')) {
         letters=QChar (0x00FA);
         letnums='G';
         verifyspecialanswer();
     }
     else{
         whatif();
     }
}

void seasonsreal::on_commandLinkButton_2_clicked()
{
    this->hide();
    Season back; 
    scoreb=0;
    totalb=0;
    scorrb=0;
    back.setModal(true);
   back.exec();
}
void seasonsreal::whatif(){
    scoreb=+(sab.size()*5)- missesc;
    QString valvb=QString::number(scoreb);
    ui->scorebox->setText(valvb);
     ui->widget_2->setVisible(false);
    //scorrb=scoreb;
    totalb=scorrb;
    if(totalb>=100){
        QMessageBox::information(
            this,
            tr("Word correctly guessed"),
            tr("For getting over 100, you must be a genius!!!") );
    }
    else if(totalb<100){
        QMessageBox::information(
            this,
            tr("Word correctly guessed"),
            tr("That was great!!!") );
    }
   ui->commandLinkButton->setVisible(true);

}

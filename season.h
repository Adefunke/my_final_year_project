#ifndef SEASON_H
#define SEASON_H

#include <QDialog>

namespace Ui {
class Season;
}

class Season : public QDialog
{
    Q_OBJECT

public:
    explicit Season(QWidget *parent = 0);
    ~Season();

private slots:
    void on_front_clicked();

    void on_back_clicked();

private:
    Ui::Season *ui;
};

#endif // SEASON_H

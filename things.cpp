#include "things.h"
#include "ui_things.h"
#include "thindsreal.h"
#include "mainwindow.h"
#include "mainthing.h"

Things::Things(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Things)
{
    ui->setupUi(this);
}

Things::~Things()
{
    delete ui;
}

void Things::on_front_clicked()
{
    this->hide();
    thindsreal front;
   front.setModal(true);
    front.exec();
}

void Things::on_back_clicked()
{
    this->hide();
    Mainthing back;
    back.setModal(true);
   back.exec();
}
